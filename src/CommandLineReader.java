import Constants.Constants;
import Exceptions.InvalidInputException;
import Models.*;
import Validators.TimeValidator;

import java.util.ArrayList;
import java.util.List;

public class CommandLineReader {
    String[] inputString;

    CommandLineReader(String[] inputString){
        this.inputString = inputString;
    }

    public void parseInput() throws Exception {
        try {
            if(inputString.length == 6) {
                List<TimeValidator> cronValidationsList = new ArrayList<>();

                Minutes minutes = new Minutes(inputString[0], Constants.MinuteMatchingPattern, Constants.MinuteMaxValue);
                Hours hours = new Hours(inputString[1], Constants.HourMatchingPattern, Constants.HoursMaxValue);
                DayOfTheMonth dayOfTheMonth = new DayOfTheMonth(inputString[2], Constants.DaysOfTheMonthMatchingPattern, Constants.DaysINMonthMaxValue);
                Month month = new Month(inputString[3], Constants.MonthMatchingPattern, Constants.MonthMaxValue);
                DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek(inputString[4], Constants.DaysOfTheWeekMatchingPattern, Constants.WeekMaxValue);


                cronValidationsList.add(minutes);
                cronValidationsList.add(hours);
                cronValidationsList.add(dayOfTheMonth);
                cronValidationsList.add(month);
                cronValidationsList.add(daysOfTheWeek);

                for(TimeValidator timeValidator: cronValidationsList){
                   timeValidator.expandedForm();
                }
                System.out.println("Command :  " + inputString[5]);
            } else {
                throw  new InvalidInputException(404, "Invalid Input String");
            }
        } catch (InvalidInputException e){
            System.out.println("Exception Raised :: " + e.getMessage());
        }
    }
}
