import java.io.File;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class CronParser {
    public static Scanner scanner;
    public static  File inputFile;
    public static void main(String[] args) throws Exception {
        try {
                inputFile = new File("/Users/atulmishra/IdeaProjects/Cron/src/TestCases.txt");
                System.out.println(inputFile);
                scanner = new Scanner(inputFile);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println("Parsing Expression :: " + line);
                    String[] cronExpression = line.split(" ");
                    CommandLineReader commandLineReader = new CommandLineReader(cronExpression);
                    commandLineReader.parseInput();
                    System.out.println();
                }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}