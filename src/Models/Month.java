package Models;

import Constants.Constants;
import Validators.TimeValidator;

public class Month extends TimeValidator {
    public Month(String months, String pattern, Integer maxValue) throws Exception {
        super(months, pattern, maxValue, Constants.Month);
        validate();
        expand();
    }

}
