package Models;

import Constants.Constants;
import Validators.TimeValidator;

public class Minutes extends TimeValidator {
    public Minutes(String minutes, String pattern, Integer maxValue) throws Exception {
        super(minutes, pattern, maxValue, Constants.Minutes);
        validate();
        expand();
    }
}
