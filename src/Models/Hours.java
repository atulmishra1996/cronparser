package Models;
import Constants.Constants;
import Validators.TimeValidator;

public class Hours extends TimeValidator { public Hours(String hours, String pattern, Integer maxValue) throws Exception {
        super(hours, pattern, maxValue, Constants.Hours);
        validate();
        expand();
    }
}
