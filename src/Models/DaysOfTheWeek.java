package Models;

import Constants.Constants;
import Validators.TimeValidator;

public class DaysOfTheWeek extends TimeValidator {
    public DaysOfTheWeek(String daysOfTheWeek, String pattern, Integer maxValue) throws Exception {
        super(daysOfTheWeek, pattern, maxValue, Constants.DayOfTheWeek);
        validate();
        expand();
    }

}
