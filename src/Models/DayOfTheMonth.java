package Models;

import Constants.Constants;
import Validators.TimeValidator;

public class DayOfTheMonth extends TimeValidator {
    public DayOfTheMonth(String daysOfMonth, String pattern, Integer maxValue) throws Exception {
        super(daysOfMonth, pattern, maxValue, Constants.DaysOfTheMonth);
        validate();
        expand();
    }

}
