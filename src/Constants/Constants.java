package Constants;

public class Constants {

    // Regex Patterns
    public static final String MinuteMatchingPattern = "^(?:\\*(?:/[0-5]?[0-9])?|/[0-5]?[0-9]|[0-5]?[0-9](?:,[0-5]?[0-9])*|([0-5]?[0-9])-(?!\\1)[0-5]?[0-9])$";
    public static final String HourMatchingPattern =   "^(\\*|(?:\\*|(?:\\*|(?:[0-9]|1[0-9]|2[0-3])))\\/(?:[0-9]|1[0-9]|2[0-3])|(?:[0-9]|1[0-9]|2[0-3])(?:(?:\\-(?:[0-9]|1[0-9]|2[0-3]))?|(?:\\,(?:[0-9]|1[0-9]|2[0-3]))*))$";
    public static final String MonthMatchingPattern = "^(\\*|(?:[1-9]|1[012])(?:(?:\\-(?:[1-9]|1[012]))?|(?:\\,(?:[1-9]|1[012]))*))$";
    public static final String DaysOfTheWeekMatchingPattern = "^(?:\\*(?:/[1-7])?|/[1-7]|[1-7](?:,[1-7])*|([1-7])-(?!\\1)[1-7](?:,([1-7])-(?!\\2)[1-7])*)$";
    public static final String DaysOfTheMonthMatchingPattern = "^(?:\\*(?:/[1-3]?[0-9])?|/[1-3]?[0-9]|[1-3]?[0-9](?:,[1-3]?[0-9])*|([1-3]?[0-9])-(?!\\1)[1-3]?[0-9])$";

    // Limits
    public static final Integer MinuteMaxValue = 60;
    public static final Integer HoursMaxValue = 24;
    public static final Integer DaysINMonthMaxValue = 31;
    public static final Integer WeekMaxValue = 7;
    public static final Integer MonthMaxValue = 13;


    //Times
    public static final String Minutes = "minutes";
    public static final String Hours = "hours";
    public static final String DayOfTheWeek = "days of the week";
    public static final String DaysOfTheMonth = "days of the month";
    public static final String Month = "month";
}
