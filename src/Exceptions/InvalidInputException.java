package Exceptions;

public class InvalidInputException extends Exception{
    public InvalidInputException(int errorCode, String errorMessage){
        super(errorMessage);
    }
}
