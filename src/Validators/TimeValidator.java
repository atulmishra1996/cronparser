package Validators;

import Constants.Constants;
import Exceptions.InvalidInputException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeValidator {
    String cronExpression;
    String regexPattern;
    public SortedSet<Integer> expandedExpression = new TreeSet<>();
    Pattern pattern;
    int maxValue;

    String timeType;

    protected TimeValidator(String expression, String pattern, int maxValue, String timeValidator){
        this.cronExpression = expression;
        this.regexPattern = pattern;
        this.maxValue = maxValue;
        this.timeType = timeValidator;
    }

    public void validate() throws Exception {
        if(cronExpression.isEmpty() || cronExpression.isBlank()){
            throw new InvalidInputException(404, "Invalid Input provided for " + timeType + " is Empty or Blank");
        }

        pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(cronExpression);
        if(!matcher.matches()){
            throw new InvalidInputException(404, "Invalid Input provided for " +timeType);
        }
    }

    public void expand() throws InvalidInputException {
        int startValue = 1;
        if(Objects.equals(timeType, Constants.Minutes) || Objects.equals(timeType, Constants.Hours)){
            startValue = 0;
            maxValue = maxValue + 1;
        }
        if (cronExpression.equals("*")) {
            for (int i = startValue; i < maxValue; i++) {
                expandedExpression.add(i);
            }
        } else if (cronExpression.startsWith("*/")) {
            int interval = Integer.parseInt(cronExpression.substring(2));
            for (int i = startValue; i < maxValue; i += interval) {
                expandedExpression.add(i);
            }
        } else if (cronExpression.startsWith("/")) {
            int interval = Integer.parseInt(cronExpression.substring(1));
            for (int i = startValue; i < maxValue; i += interval) {
                expandedExpression.add(i);
            }
        } else if (cronExpression.contains(",")) {
            String[] minuteTokens = cronExpression.split(",");
            for (String token : minuteTokens) {
                expandedExpression.add(Integer.parseInt(token));
            }
        } else if (cronExpression.contains("-")) {
            String[] rangeTokens = cronExpression.split("-");
            int start = Integer.parseInt(rangeTokens[0]);
            int end = Integer.parseInt(rangeTokens[1]);
            if(end <= start){
                throw new InvalidInputException(404, "Invalid Range mentioned in "+ timeType);
            }
            for (int i = start; i <= end; i++) {
                expandedExpression.add(i);
            }
        } else {
            expandedExpression.add(Integer.parseInt(cronExpression));
        }
    }

    public void expandedForm(){
        System.out.print(timeType + ":  ");

        for(Integer i: expandedExpression){
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
